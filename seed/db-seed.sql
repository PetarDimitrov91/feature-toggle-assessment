SET SESSION sql_mode= '';

DROP TABLE IF EXISTS `customer_ids`;
DROP TABLE IF EXISTS `feature-toggle`;

CREATE TABLE `feature-toggle` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `display_name` VARCHAR(255),
  `technical_name` VARCHAR(255) NOT NULL,
  `expires_on` DATETIME,
  `description` VARCHAR(1000),
  `inverted` BOOLEAN NOT NULL,
  `archived` BOOLEAN DEFAULT FALSE,
  `active` BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;


CREATE TABLE `customer_ids` (
  `feature_toggle_id` BIGINT,
  `customer_id` INT,
  FOREIGN KEY (`feature_toggle_id`) REFERENCES `feature-toggle`(`id`)
);

INSERT INTO `feature-toggle` (`display_name`, `technical_name`, `expires_on`, `description`, `inverted`)
VALUES ('UI-Theming', 'ui-theming', '2025-12-31 23:59:59', 'This feature toggle is for new UI Theming. You can chose between 4 themes and light and dark mode.', false),
       ('AI-Chat', 'ai-chat', '2025-12-31 23:59:59', 'This feature toggle is for a chat feature where the users can communicate with AI assistant.', false);

SET @ui_theming_id = (SELECT id FROM `feature-toggle` WHERE `technical_name` = 'ui-theming');
SET @ai_chat_id = (SELECT id FROM `feature-toggle` WHERE `technical_name` = 'ai-chat');

INSERT INTO `customer_ids` (`feature_toggle_id`, `customer_id`)
VALUES (@ui_theming_id, 123),
       (@ui_theming_id, 456),
       (@ai_chat_id, 789),
       (@ai_chat_id, 012);
