FROM node:20 AS base
WORKDIR /app


# Build target dependencies #
FROM base AS dependencies
COPY package.json yarn.lock ./
RUN yarn

FROM dependencies AS builder
COPY . .

RUN rm -rf .nx
RUN rm -rf .angular

RUN npx nx run feature-toggle-client:build:production

# Build target production ssr #
FROM base AS production-ssr
COPY --from=builder /app/dist /app/dist
COPY --from=dependencies /app/node_modules /app/node_modules

USER 1000:1001

EXPOSE 4200

CMD ["node", "dist/client/apps/feature-toggle-client/server/server.mjs"]
