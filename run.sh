#!/bin/bash

cd containers

docker-compose -f app-compose.yml up -d

#Go back
cd ..

# Wait for the MySQL container to fully start, adjust the sleep time as needed
echo "Waiting for MySQL to fully start, sleeping for 30s..."
sleep 30

# Seed the database
docker cp seed/db-seed.sql db:/db-seed.sql
docker exec -it db bash -c "mysql -u user -ppassword assessment < /db-seed.sql"

echo "Database seeding completed."
