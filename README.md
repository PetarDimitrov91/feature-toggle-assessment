# Feature Toggle assessment

## Introduction

This is a simple project to demonstrate the use of feature toggles in a Spring Boot & Angular application. The project is divided into two parts:

1. A Spring Boot application that exposes a REST API to manage feature toggles.
2. An Angular application that consumes the REST API to enable/disable features.

### Architecture

This is project is a NX Monorepo. The project is divided into two parts: Front-End and Back-End. The Front-End is located in **/client** folder and the Back-End is located in **/server** folder.
There is also a **/containers** folder that contains the docker-compose file to run the application.

#### Back-End

The Back-End is a simple Monolithic Spring Boot application that exposes a REST API to manage feature toggles. The application uses a MySQL database to store the feature toggles.
The architecture of the backend designed ensures the MVC pattern.

**Technologies**
Java 21
Spring Boot
Spring Data JPA
MySQL
Lombok
Swagger
OpenAPI
Docker

#### Front-End

The Front-End is a simple Angular SSR application that consumes the REST API to enable/disable features.
It follows the DDD (Domain-Driven Design) pattern and is divided into feature libs.

**Technologies**
Angular 17
Angular SSR
Angular Material
RxJS
NX
Docker

## Usage

**Prerequisites**

1. Java 21
2. Node 20
3. Docker
4. I prefer to use Yarn as a package manager, but you can use npm as well.

**Option 1 - the easiest approach:**
Just navigate to the root of the project and run the run.sh scrip to bootstrap the Database, DB-Client, The Front-End and the Back-End.

```bash
 ./run.sh
 ```

It is important to run the script because it also seeds the database with some initial data.

#### Navigate to `http://localhost:4200` to access the application.

#### Navigate to `http://localhost:8080/swagger-ui/index.html` to access the API documentation.

#### Navigate to `http://localhost:8081/` to access the MySQL client (phpmyadmin). The username is `user` and the password is `password`.

**Option 2 - the manual approach:**

1. Setup DB and configure it in the application.properties file. (You can use the docker-compose file in the containers folder to run the MySQL database)
2. Build and run the Back-End application. Use the following commands:

```bash
cd server/feature-toggle
./mvnw clean install
cd target
java -jar feature-toggle-0.0.1-SNAPSHOT.jar
```

3. Build and run the Front-End application. In the root of the project run:

```bash
  yarn install
  yarn serve
```

### Misc

Feel free to explore the project and run the other scripts which are defined in the package.json file.
For example run: ```yarn graph``` to exlore the dependency graph of the Front-End app.

### Contributing

Petar Dimitrov
