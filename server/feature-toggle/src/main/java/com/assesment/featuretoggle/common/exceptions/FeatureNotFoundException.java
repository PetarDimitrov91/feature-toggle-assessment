package com.assesment.featuretoggle.common.exceptions;

import static com.assesment.featuretoggle.utils.ExceptionMessages.FEATURE_NOT_FOUND;

public class FeatureNotFoundException extends FeatureBaseException {
  public FeatureNotFoundException() {
    super(FEATURE_NOT_FOUND);
  }
}
