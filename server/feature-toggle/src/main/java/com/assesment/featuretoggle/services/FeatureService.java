package com.assesment.featuretoggle.services;

import com.assesment.featuretoggle.common.exceptions.FeatureNotFoundException;
import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import com.assesment.featuretoggle.repositories.FeatureRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class FeatureService {
  private final FeatureRepository featureRepository;

  /**
   * Fetches all Feature entities that are not archived, with pagination.
   *
   * @param page the page number to fetch.
   * @param size the number of Feature entities per page.
   * @return a Page of Feature entities that are not archived.
   */
  public Page<Feature> getAllFeatures(int page, int size) {
    Specification<Feature> notArchived = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("archived"), false);

    return this.featureRepository.findAll(notArchived, PageRequest.of(page, size));
  }

  /**
   * Fetches a Feature entity by its ID.
   *
   * @param id the ID of the Feature entity to fetch.
   * @return the Feature entity with the given ID.
   * @throws RuntimeException if no Feature entity with the given ID is found.
   */
  public Feature getFeatureById(Long id) {
    return featureRepository.findById(id).orElseThrow(FeatureNotFoundException::new);
  }

  /**
   * Creates a new Feature entity.
   *
   * @param feature the DTO containing the data to create the new Feature entity.
   * @return the created Feature entity.
   */
  public Feature createFeature(CreateFeatureDto feature) {
    return featureRepository.save(feature.mapToFeature());
  }

  /**
   * Deletes a Feature entity by its ID.
   *
   * @param id the ID of the Feature entity to delete.
   */
  public void deleteFeature(Long id) {
    Feature feature = this.getFeatureById(id);
    featureRepository.deleteById(feature.getId());
  }

  /**
   * Updates a Feature entity by its ID.
   *
   * @param id      the ID of the Feature entity to update.
   * @param feature the DTO containing the data to update the Feature entity.
   * @return the updated Feature entity.
   * @throws RuntimeException if no Feature entity with the given ID is found.
   */
  public Feature updateFeature(Long id, CreateFeatureDto feature) {
    Feature feat = featureRepository.findById(id).orElseThrow(FeatureNotFoundException::new);

    feat.setDisplayName(feature.getDisplayName());
    feat.setTechnicalName(feature.getTechnicalName());
    feat.setExpiresOn(feature.getExpiresOn());
    feat.setDescription(feature.getDescription());
    feat.setInverted(feature.isInverted());
    feat.setCustomerIds(feature.getCustomerIds());

    return featureRepository.save(feat);
  }

  /**
   * Adds customer IDs to a Feature entity.
   *
   * @param featureId   the ID of the Feature entity to add customer IDs to.
   * @param customerIds the customer IDs to add.
   * @return the updated Feature entity.
   * @throws RuntimeException if no Feature entity with the given ID is found.
   */
  public Feature addCustomersForFeature(Long featureId, List<Long> customerIds) {
    Feature feature = featureRepository.findById(featureId).orElseThrow(FeatureNotFoundException::new);

    if (feature.getCustomerIds() == null) {
      feature.setCustomerIds(new ArrayList<>());
    }

    feature.getCustomerIds().addAll(customerIds);

    return featureRepository.save(feature);
  }

  /**
   * Adds Feature entities to a customer.
   *
   * @param customerId the ID of the customer to add Feature entities to.
   * @param featureIds the IDs of the Feature entities to add.
   * @return the updated Feature entities.
   */
  public List<Feature> addFeaturesForCustomer(Long customerId, List<Long> featureIds) {
    List<Feature> features = featureRepository.findAllById(featureIds);
    features.forEach(feature -> {

      if (feature.getCustomerIds() == null) {
        feature.setCustomerIds(new ArrayList<>());
      }

      feature.getCustomerIds().add(customerId);
    });

    return featureRepository.saveAll(features);
  }

  /**
   * Inverts the 'inverted' property of a Feature entity.
   *
   * @param id       the ID of the Feature entity to invert.
   * @param inverted the new value for the 'inverted' property.
   * @return the updated Feature entity.
   * @throws RuntimeException if no Feature entity with the given ID is found.
   */
  public Feature invertFeature(Long id, boolean inverted) {
    Feature feature = featureRepository.findById(id).orElseThrow(FeatureNotFoundException::new);
    feature.setInverted(inverted);

    return featureRepository.save(feature);
  }

  /**
   * Archives a Feature entity.
   *
   * @param id the ID of the Feature entity to archive.
   * @return the updated Feature entity.
   * @throws RuntimeException if no Feature entity with the given ID is found.
   */
  public Feature archiveFeature(Long id) {
    Feature feature = featureRepository.findById(id).orElseThrow(FeatureNotFoundException::new);
    feature.setArchived(true);

    return featureRepository.save(feature);
  }

  /**
   * Toggles a feature.
   *
   * @param id     the ID of the Feature entity to toggle.
   * @param active the new value for the 'active' property.
   * @return the updated Feature entity.
   */
  public Feature toggleFeature(Long id, boolean active) {
    Feature feature = featureRepository.findById(id).orElseThrow(FeatureNotFoundException::new);
    feature.setActive(active);

    return featureRepository.save(feature);
  }
}
