package com.assesment.featuretoggle.controllers;

import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import com.assesment.featuretoggle.services.FeatureService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController()
@Tag(name = "Feature Toggle", description = "Feature Toggle API")
@ApiResponses(value = {
  @ApiResponse(responseCode = "404", description = "Not Found"),
  @ApiResponse(responseCode = "500", description = "Internal Server Error"),
  @ApiResponse(responseCode = "503", description = "Service unavailable")
})
@RequestMapping("/api/v1/features")
public class FeatureController {
  private final FeatureService featureService;

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Retrieves a page of features"),
    @ApiResponse(responseCode = "400", description = "Invalid page or size value"),
  })
  @Parameter(name = "page", description = "Page number")
  @Parameter(name = "size", description = "Number of items per page")
  @CrossOrigin(origins = "*")
  @GetMapping("all")
  public ResponseEntity<Page<Feature>> getAllFeatures(
    @RequestParam(name = "page", defaultValue = "0") int page,
    @RequestParam(name = "size", defaultValue = "10") int size
  ) {
    Page<Feature> features = featureService.getAllFeatures(page, size);
    return ResponseEntity.ok(features);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Retrieves feature by id"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @CrossOrigin(origins = "*")
  @GetMapping("/{id}")
  public ResponseEntity<Feature> getFeatureById(@PathVariable Long id) {
    Feature feature = featureService.getFeatureById(id);
    return ResponseEntity.ok(feature);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "201", description = "Creates a new feature"),
  })
  @Parameter(name = "feature", description = "Feature object", required = true)
  @CrossOrigin(origins = "*")
  @PostMapping("/create")
  public ResponseEntity<Feature> createFeature(@RequestBody CreateFeatureDto feature) {
    Feature feat = featureService.createFeature(feature);
    return new ResponseEntity<>(feat, HttpStatus.CREATED);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Updates a feature"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @Parameter(name = "feature", description = "Feature object", required = true)
  @CrossOrigin(origins = "*")
  @PutMapping("/{id}")
  public ResponseEntity<Feature> updateFeature(@PathVariable Long id, @RequestBody CreateFeatureDto feature) {
    Feature feat = featureService.updateFeature(id, feature);
    return ResponseEntity.ok(feat);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "204", description = "Deletes a feature"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @CrossOrigin(origins = "*")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteFeature(@PathVariable Long id) {
    featureService.deleteFeature(id);
    return ResponseEntity.noContent().build();
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Adds customers for a feature"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @CrossOrigin(origins = "*")
  @PostMapping("/{featureId}/addCustomers")
  public ResponseEntity<Feature> addCustomersToFeature(@PathVariable Long featureId, @RequestBody List<Long> customerIds) {
    Feature feature = featureService.addCustomersForFeature(featureId, customerIds);
    return ResponseEntity.ok(feature);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Adds customers for a feature"),
  })
  @Parameter(name = "id", description = "Customer id", required = true)
  @CrossOrigin(origins = "*")
  @PostMapping("/{customerId}/addFeatures")
  public ResponseEntity<List<Feature>> addFeaturesForCustomers(@PathVariable Long customerId, @RequestBody List<Long> featureIds) {
    List<Feature> feature = featureService.addFeaturesForCustomer(customerId, featureIds);
    return ResponseEntity.ok(feature);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Inverts a feature"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @Parameter(name = "inverted", description = "Inverted value", required = true)
  @CrossOrigin(origins = "*")
  @PatchMapping("/{id}/invert")
  public ResponseEntity<Feature> invertFeature(@PathVariable Long id, @RequestParam boolean inverted) {
    Feature feature = featureService.invertFeature(id, inverted);
    return ResponseEntity.ok(feature);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Archives a feature"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @CrossOrigin(origins = "*")
  @PatchMapping("/{id}/archive")
  public ResponseEntity<Feature> archiveFeature(@PathVariable Long id) {
    Feature feature = featureService.archiveFeature(id);
    return ResponseEntity.ok(feature);
  }

  @ApiResponses(value = {
    @ApiResponse(responseCode = "200", description = "Toggles a feature"),
  })
  @Parameter(name = "id", description = "Feature id", required = true)
  @Parameter(name = "active", description = "Active value", required = true)
  @CrossOrigin(origins = "*")
  @PatchMapping("/{id}/toggle")
  public ResponseEntity<Feature> toggleFeature(@PathVariable Long id, @RequestParam boolean active) {
    Feature feature = featureService.toggleFeature(id, active);
    return ResponseEntity.ok(feature);
  }
}
