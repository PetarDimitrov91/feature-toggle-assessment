package com.assesment.featuretoggle.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

import static org.springframework.http.HttpMethod.*;

@Configuration
@EnableWebMvc
@Profile({"development", "production"})
public class WebConfig {
  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.applyPermitDefaultValues();

    configuration.addAllowedMethod(GET);
    configuration.addAllowedMethod(POST);
    configuration.addAllowedMethod(PATCH);
    configuration.addAllowedMethod(PUT);
    configuration.addAllowedMethod(DELETE);
    configuration.addAllowedMethod(OPTIONS);
    configuration.addAllowedMethod(HEAD);

    configuration.setExposedHeaders(List.of("*"));

    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);

    return source;
  }
}

