package com.assesment.featuretoggle.models.feature_toggle;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@ToString
@Table(name = "feature-toggle")
public class Feature {
  @Id
  @Column(name = "id", insertable = false, updatable = false, nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "display_name")
  private String displayName;

  @Column(name = "technical_name", nullable = false, unique = true)
  private String technicalName;

  @Column(name = "expires_on")
  private Date expiresOn;

  @Column(name = "description", length = 1000)
  private String description;

  @Column(name = "inverted", nullable = false)
  private boolean inverted = false;

  @Column(name = "archived", nullable = false)
  private boolean archived = false;

  @Column(name = "active", nullable = false)
  private boolean active = true;

  @ElementCollection
  @CollectionTable(name = "customer_ids", joinColumns = @JoinColumn(name = "feature_toggle_id"))
  @Column(name = "customer_id")
  private List<Long> customerIds;

}
