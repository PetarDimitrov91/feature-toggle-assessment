package com.assesment.featuretoggle.common.exceptions;

public class FeatureBaseException extends RuntimeException {
  public FeatureBaseException(String message) {
    super(message);
  }
}
