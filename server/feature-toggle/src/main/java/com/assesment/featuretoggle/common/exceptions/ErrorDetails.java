package com.assesment.featuretoggle.common.exceptions;

import java.util.Date;

public record ErrorDetails(Date timestamp, String message, String exceptionName) {

}
