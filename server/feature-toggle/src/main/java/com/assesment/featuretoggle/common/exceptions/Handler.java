package com.assesment.featuretoggle.common.exceptions;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class Handler {
  @ExceptionHandler(FeatureNotFoundException.class)
  public ResponseEntity<ErrorDetails> dataNotFoundExceptionHandling(Exception exception) {
    return new ResponseEntity<>(genErrDetails(exception), NOT_FOUND);
  }

  private ErrorDetails genErrDetails(Exception exception) {
    return new ErrorDetails(
      new Date(),
      exception.getMessage(),
      exception.getClass().getSimpleName());
  }
}
