package com.assesment.featuretoggle.models.feature_toggle;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Builder
@Getter
@Setter
public class CreateFeatureDto {
  @JsonProperty
  @Nullable
  private String displayName;

  @JsonProperty
  @NotNull
  private String technicalName;

  @JsonProperty
  @Nullable
  private Date expiresOn;

  @JsonProperty
  @Nullable
  private String description;

  @JsonProperty
  @NotNull
  private boolean inverted;

  @JsonProperty
  @NotNull
  private List<Long> customerIds;

  public Feature mapToFeature() {
    return Feature.builder()
      .displayName(displayName)
      .technicalName(technicalName)
      .expiresOn(expiresOn)
      .description(description)
      .inverted(inverted)
      .customerIds(customerIds)
      .build();
  }
}
