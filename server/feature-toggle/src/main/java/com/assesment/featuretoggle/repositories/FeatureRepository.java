package com.assesment.featuretoggle.repositories;

import com.assesment.featuretoggle.models.feature_toggle.Feature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeatureRepository extends JpaRepository<Feature, Long>, JpaSpecificationExecutor<Feature> {
  //Custom Query example using HQL
  @Query("SELECT f FROM Feature f WHERE f.archived = true")
  List<Feature> getAllArchived();
}
