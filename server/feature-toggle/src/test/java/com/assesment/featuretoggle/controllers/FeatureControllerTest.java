package com.assesment.featuretoggle.controllers;

import com.assesment.featuretoggle.MockData;
import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import com.assesment.featuretoggle.services.FeatureService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(FeatureController.class)
public class FeatureControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private FeatureService featureService;

  private Feature MOCK_FEATURE_1;
  private Feature MOCK_FEATURE_2;
  private Feature MOCK_FEATURE_3;
  private Page<Feature> MOCK_FIRST_PAGE;

  @BeforeEach
  public void setUp() {
    this.MOCK_FEATURE_1 = Feature.builder()
      .id(MockData.MOCK_FEATURE_1.getId())
      .displayName(MockData.MOCK_FEATURE_1.getDisplayName())
      .technicalName(MockData.MOCK_FEATURE_1.getTechnicalName())
      .expiresOn(MockData.MOCK_FEATURE_1.getExpiresOn())
      .description(MockData.MOCK_FEATURE_1.getDescription())
      .inverted(MockData.MOCK_FEATURE_1.isInverted())
      .archived(MockData.MOCK_FEATURE_1.isArchived())
      .active(MockData.MOCK_FEATURE_1.isActive())
      .customerIds(MockData.MOCK_FEATURE_1.getCustomerIds())
      .build();

    this.MOCK_FEATURE_2 = Feature.builder()
      .id(MockData.MOCK_FEATURE_2.getId())
      .displayName(MockData.MOCK_FEATURE_2.getDisplayName())
      .technicalName(MockData.MOCK_FEATURE_2.getTechnicalName())
      .expiresOn(MockData.MOCK_FEATURE_2.getExpiresOn())
      .description(MockData.MOCK_FEATURE_2.getDescription())
      .inverted(MockData.MOCK_FEATURE_2.isInverted())
      .archived(MockData.MOCK_FEATURE_2.isArchived())
      .active(MockData.MOCK_FEATURE_2.isActive())
      .customerIds(MockData.MOCK_FEATURE_2.getCustomerIds())
      .build();

    this.MOCK_FEATURE_3 = Feature.builder()
      .id(MockData.MOCK_FEATURE_3.getId())
      .displayName(MockData.MOCK_FEATURE_3.getDisplayName())
      .technicalName(MockData.MOCK_FEATURE_3.getTechnicalName())
      .expiresOn(MockData.MOCK_FEATURE_3.getExpiresOn())
      .description(MockData.MOCK_FEATURE_3.getDescription())
      .inverted(MockData.MOCK_FEATURE_3.isInverted())
      .archived(MockData.MOCK_FEATURE_3.isArchived())
      .active(MockData.MOCK_FEATURE_3.isActive())
      .customerIds(MockData.MOCK_FEATURE_3.getCustomerIds())
      .build();

    this.MOCK_FIRST_PAGE = MockData.getFeaeturePage(0, 10);
  }

  // ############################## GET /api/v1/features/all ##############################

  @Test
  public void getAllFeaturesEndpointShould_BeCalledCorrectlyWithDefaultPageAndSize() throws Exception {
    when(featureService.getAllFeatures(0, 10)).thenReturn(this.MOCK_FIRST_PAGE);

    MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").isArray())
      .andReturn();

    verify(featureService, times(1)).getAllFeatures(0, 10);
  }

  @Test
  public void getAllFeaturesEndpoint_ShouldBeCalledCorrectlyWithPassedPageAndSize() throws Exception {
    when(featureService.getAllFeatures(1, 20)).thenReturn(this.MOCK_FIRST_PAGE);

    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all?page=1&size=20"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").isArray());

    verify(featureService, times(1)).getAllFeatures(1, 20);
  }

  @Test
  public void getAllFeaturesEndpoint_ShouldBeReturnSatatusBadRequestWhenPageIsIvalidValue() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all?page=dsa&size=10"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  @Test
  public void getAllFeaturesEndpoint_ShouldBeReturnSatatusBadRequestWhenSizeIsIvalidValue() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all?page=1&size=1fs"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  // ############################## GET /api/v1/features/{id} ##############################

  @Test
  public void getFeatureByIdEndpoint_ShouldBeCalledCorrectly() throws Exception {
    when(featureService.getFeatureById(1L)).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/1"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.displayName").value(this.MOCK_FEATURE_1.getDisplayName()))
      .andExpect(jsonPath("$.technicalName").value(this.MOCK_FEATURE_1.getTechnicalName()))
      .andExpect(jsonPath("$.description").value(this.MOCK_FEATURE_1.getDescription()))
      .andExpect(jsonPath("$.inverted").value(this.MOCK_FEATURE_1.isInverted()))
      .andExpect(jsonPath("$.archived").value(this.MOCK_FEATURE_1.isArchived()))
      .andExpect(jsonPath("$.active").value(this.MOCK_FEATURE_1.isActive()));

    verify(featureService, times(1)).getFeatureById(1L);
  }

  // ############################## POST /api/v1/features/create ##############################

  @Test
  public void createFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    when(featureService.createFeature(any(CreateFeatureDto.class))).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/create")
        .contentType("application/json")
        .content("{\"displayName\":\"Test Feature\",\"technicalName\":\"test_feature\",\"description\":\"This is a test feature\",\"inverted\":false,\"customerIds\":[1,2,3]}"))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.displayName").value(this.MOCK_FEATURE_1.getDisplayName()))
      .andExpect(jsonPath("$.technicalName").value(this.MOCK_FEATURE_1.getTechnicalName()))
      .andExpect(jsonPath("$.description").value(this.MOCK_FEATURE_1.getDescription()))
      .andExpect(jsonPath("$.inverted").value(this.MOCK_FEATURE_1.isInverted()))
      .andExpect(jsonPath("$.archived").value(this.MOCK_FEATURE_1.isArchived()))
      .andExpect(jsonPath("$.active").value(this.MOCK_FEATURE_1.isActive()));

    verify(featureService, times(1)).createFeature(any(CreateFeatureDto.class));
  }

  @Test
  public void createFeatureEndpoint_shouldThrowBadRequestWhenBodyIsMissing() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/create"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  // ############################## PUT /api/v1/features/{id} ##############################

  @Test
  public void updateFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    when(featureService.updateFeature(anyLong(), any(CreateFeatureDto.class))).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/features/1")
        .contentType("application/json")
        .content("{\"displayName\":\"Test Feature\",\"technicalName\":\"test_feature\",\"description\":\"This is a test feature\",\"inverted\":false,\"customerIds\":[1,2,3]}"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.displayName").value(this.MOCK_FEATURE_1.getDisplayName()))
      .andExpect(jsonPath("$.technicalName").value(this.MOCK_FEATURE_1.getTechnicalName()))
      .andExpect(jsonPath("$.description").value(this.MOCK_FEATURE_1.getDescription()))
      .andExpect(jsonPath("$.inverted").value(this.MOCK_FEATURE_1.isInverted()))
      .andExpect(jsonPath("$.archived").value(this.MOCK_FEATURE_1.isArchived()))
      .andExpect(jsonPath("$.active").value(this.MOCK_FEATURE_1.isActive()));

    verify(featureService, times(1)).updateFeature(anyLong(), any(CreateFeatureDto.class));
  }

  @Test
  public void updateFeatureEndpoint_shouldThrowBadRequestWhenBodyIsMissing() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/features/1"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  // ############################## DELETE /api/v1/features/{id} ##############################

  @Test
  public void deleteFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    doNothing().when(featureService).deleteFeature(1L);

    mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/features/1"))
      .andDo(print())
      .andExpect(status().isNoContent());

    verify(featureService, times(1)).deleteFeature(1L);
  }

  // ############################## POST /api/v1/features/{featureId}/addCustomers ##############################

  @Test
  public void addCustomersToFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    List<Long> ids = List.of(1L, 2L, 3L);
    this.MOCK_FEATURE_1.setCustomerIds(ids);

    when(featureService.addCustomersForFeature(1L, ids)).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/1/addCustomers")
        .contentType("application/json")
        .content("[1,2,3]"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.customerIds").isArray())
      .andExpect(jsonPath("$.customerIds").isNotEmpty());

    verify(featureService, times(1)).addCustomersForFeature(1L, List.of(1L, 2L, 3L));
  }

// ############################## POST /api/v1/features/{customerId}/addFeatures ##############################

  @Test
  public void addFeaturesForCustomersEndpoint_ShouldBeCalledCorrectly() throws Exception {
    List<Long> ids = List.of(1L, 2L, 3L);
    this.MOCK_FEATURE_1.setCustomerIds(ids);
    this.MOCK_FEATURE_2.setCustomerIds(ids);
    this.MOCK_FEATURE_3.setCustomerIds(ids);

    when(featureService.addFeaturesForCustomer(1L, ids)).thenReturn(List.of(this.MOCK_FEATURE_1, this.MOCK_FEATURE_2, this.MOCK_FEATURE_3));

    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/1/addFeatures")
        .contentType("application/json")
        .content("[1,2,3]"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isArray())
      .andExpect(jsonPath("$").isNotEmpty());

    verify(featureService, times(1)).addFeaturesForCustomer(1L, List.of(1L, 2L, 3L));
  }

  // ############################## POST /api/v1/features/{id}/invert ##############################

  @Test
  public void invertFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    this.MOCK_FEATURE_1.setInverted(true);

    when(featureService.invertFeature(1L, true)).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/1/invert?inverted=true"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.inverted").value(true));

    verify(featureService, times(1)).invertFeature(1L, true);
  }

  @Test
  public void invertFeatureEndpoint_ShouldThrowBadRequestWhenInvertedIsMissing() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/1/invert"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  @Test
  public void invertFeatureEndpoint_ShouldThrowBadRequestWhenInvertedIsInvalid() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/1/invert?inverted=dsa"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  // ############################## POST /api/v1/features/{id}/archive ##############################

  @Test
  public void archiveFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    this.MOCK_FEATURE_1.setArchived(true);

    when(featureService.archiveFeature(1L)).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/1/archive"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.archived").value(true));

    verify(featureService, times(1)).archiveFeature(1L);
  }

  // ############################## POST /api/v1/features/{id}/toggle ##############################

  @Test
  public void toggleFeatureEndpoint_ShouldBeCalledCorrectly() throws Exception {
    this.MOCK_FEATURE_1.setActive(false);

    when(featureService.toggleFeature(1L, false)).thenReturn(this.MOCK_FEATURE_1);

    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/1/toggle?active=false"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(1))
      .andExpect(jsonPath("$.active").value(false));

    verify(featureService, times(1)).toggleFeature(1L, false);
  }

  @Test
  public void toggleFeatureEndpoint_ShouldThrowBadRequestWhenActiveIsMissing() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/1/toggle"))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }
}
