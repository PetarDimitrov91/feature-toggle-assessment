package com.assesment.featuretoggle;

import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public class MockData {
  public static final Feature MOCK_FEATURE_1 = Feature.builder()
    .id(1L)
    .displayName("Test Feature")
    .technicalName("test_feature")
    .expiresOn(null)
    .description("A test feature")
    .inverted(false)
    .archived(false)
    .active(true)
    .customerIds(null)
    .build();

  public static final Feature MOCK_FEATURE_2 = Feature.builder()
    .id(2L)
    .displayName("Test Feature 2")
    .technicalName("test_feature_2")
    .expiresOn(null)
    .description("A test feature 2")
    .inverted(false)
    .archived(false)
    .active(true)
    .customerIds(null)
    .build();

  public static final Feature MOCK_FEATURE_3 = Feature.builder()
    .id(3L)
    .displayName("Test Feature 3")
    .technicalName("test_feature_3")
    .expiresOn(null)
    .description("A test feature 3")
    .inverted(false)
    .archived(false)
    .active(true)
    .customerIds(null)
    .build();

  public static final CreateFeatureDto MOCK_CREATE_FEATURE_DTO = CreateFeatureDto.builder()
    .displayName("Test Feature")
    .technicalName("test_feature")
    .expiresOn(null)
    .description("A test feature")
    .inverted(false)
    .customerIds(null)
    .build();

  public static final List<Feature> MOCK_FEATURES = List.of(MOCK_FEATURE_1, MOCK_FEATURE_2, MOCK_FEATURE_3);

  public static Page<Feature> getFeaeturePage(int page, int size) {
    PageRequest pageRequest = PageRequest.of(page, size);
    return new PageImpl<>(MOCK_FEATURES, pageRequest, MOCK_FEATURES.size());
  }
}
