package com.assesment.featuretoggle.repository;

import com.assesment.featuretoggle.MockData;
import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import com.assesment.featuretoggle.repositories.FeatureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@DataJpaTest
@AutoConfigureMockMvc
public class FeatureRepositoryTest {
  @Autowired
  private FeatureRepository featureRepository;


  private Feature MOCK_FEATURE_1;
  private CreateFeatureDto MOCK_CREATE_FEATURE_DTO;


  @BeforeEach
  public void setUp() {
    this.MOCK_FEATURE_1 = Feature.builder()
      .id(MockData.MOCK_FEATURE_1.getId())
      .displayName(MockData.MOCK_FEATURE_1.getDisplayName())
      .technicalName(MockData.MOCK_FEATURE_1.getTechnicalName())
      .expiresOn(MockData.MOCK_FEATURE_1.getExpiresOn())
      .description(MockData.MOCK_FEATURE_1.getDescription())
      .inverted(MockData.MOCK_FEATURE_1.isInverted())
      .archived(MockData.MOCK_FEATURE_1.isArchived())
      .active(MockData.MOCK_FEATURE_1.isActive())
      .customerIds(MockData.MOCK_FEATURE_1.getCustomerIds())
      .build();

    this.MOCK_CREATE_FEATURE_DTO = CreateFeatureDto.builder()
      .displayName(MockData.MOCK_CREATE_FEATURE_DTO.getDisplayName())
      .technicalName(MockData.MOCK_CREATE_FEATURE_DTO.getTechnicalName())
      .expiresOn(MockData.MOCK_CREATE_FEATURE_DTO.getExpiresOn())
      .description(MockData.MOCK_CREATE_FEATURE_DTO.getDescription())
      .inverted(MockData.MOCK_CREATE_FEATURE_DTO.isInverted())
      .customerIds(MockData.MOCK_CREATE_FEATURE_DTO.getCustomerIds())
      .build();
  }

  @Test
  @Transactional
  public void testFeatureCreation() {
    Feature feat = MOCK_CREATE_FEATURE_DTO.mapToFeature();
    Feature feature = this.featureRepository.save(feat);

    assertThat(feature.getId()).isNotNull();
    assertThat(feature.getDisplayName()).isEqualTo(MOCK_CREATE_FEATURE_DTO.getDisplayName());
    assertThat(feature.getTechnicalName()).isEqualTo(MOCK_CREATE_FEATURE_DTO.getTechnicalName());
    assertThat(feature.getExpiresOn()).isEqualTo(MOCK_CREATE_FEATURE_DTO.getExpiresOn());
    assertThat(feature.getDescription()).isEqualTo(MOCK_CREATE_FEATURE_DTO.getDescription());
    assertThat(feature.isInverted()).isEqualTo(MOCK_CREATE_FEATURE_DTO.isInverted());
    assertThat(feature.getCustomerIds()).isEqualTo(MOCK_CREATE_FEATURE_DTO.getCustomerIds());
  }

  @Test
  @Transactional
  public void testFeatureDeletion() {
    Feature feature = this.featureRepository.save(MOCK_FEATURE_1);
    this.featureRepository.deleteById(feature.getId());

    assertThat(this.featureRepository.findById(feature.getId())).isEmpty();
  }

  @Test
  @Transactional
  public void testFeatureUpdate() {
    Feature feature = this.featureRepository.save(MOCK_CREATE_FEATURE_DTO.mapToFeature());

    CreateFeatureDto updatedDto = CreateFeatureDto.builder()
      .displayName("Updated Display Name")
      .technicalName("Updated Technical Name")
      .expiresOn(null)
      .description("Updated Description")
      .inverted(false)
      .customerIds(List.of(1L, 2L, 3L))
      .build();

    this.featureRepository.findById(feature.getId()).ifPresent(feat -> {
      feat.setDisplayName(updatedDto.getDisplayName());
      feat.setTechnicalName(updatedDto.getTechnicalName());
      feat.setExpiresOn(updatedDto.getExpiresOn());
      feat.setDescription(updatedDto.getDescription());
      feat.setInverted(updatedDto.isInverted());

      if (updatedDto.getCustomerIds() != null) {
        if (feat.getCustomerIds() == null) {
          feat.setCustomerIds(new ArrayList<>());
        }

        feat.getCustomerIds().addAll(updatedDto.getCustomerIds());
      }

      this.featureRepository.save(feat);
    });

    Feature updatedFeature = this.featureRepository.findById(feature.getId()).orElse(null);

    assertNotNull(updatedFeature);
    assertNotNull(updatedFeature.getDisplayName());
    assertThat(updatedFeature.getDisplayName()).isEqualTo(updatedDto.getDisplayName());
    assertThat(updatedFeature.getTechnicalName()).isEqualTo(updatedDto.getTechnicalName());
    assertThat(updatedFeature.getExpiresOn()).isEqualTo(updatedDto.getExpiresOn());
    assertThat(updatedFeature.getDescription()).isEqualTo(updatedDto.getDescription());
    assertThat(updatedFeature.isInverted()).isEqualTo(updatedDto.isInverted());
    assertThat(updatedFeature.getCustomerIds()).isEqualTo(updatedDto.getCustomerIds());
  }
}
