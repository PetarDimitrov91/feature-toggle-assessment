package com.assesment.featuretoggle.services;

import com.assesment.featuretoggle.MockData;
import com.assesment.featuretoggle.common.exceptions.FeatureNotFoundException;
import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import com.assesment.featuretoggle.repositories.FeatureRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.assesment.featuretoggle.utils.ExceptionMessages.FEATURE_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {FeatureService.class})
class FeatureServiceTest {
  @MockBean
  private FeatureRepository featureRepository;

  @Autowired
  private FeatureService featureService;


  private Feature MOCK_FEATURE_1;
  private CreateFeatureDto MOCK_CREATE_FEATURE_DTO;
  private List<Feature> MOCK_FEATURES_LIST;
  private Page<Feature> MOCK_FIRST_PAGE;

  @BeforeEach
  public void setUp() {
    this.MOCK_FEATURE_1 = Feature.builder()
      .id(MockData.MOCK_FEATURE_1.getId())
      .displayName(MockData.MOCK_FEATURE_1.getDisplayName())
      .technicalName(MockData.MOCK_FEATURE_1.getTechnicalName())
      .expiresOn(MockData.MOCK_FEATURE_1.getExpiresOn())
      .description(MockData.MOCK_FEATURE_1.getDescription())
      .inverted(MockData.MOCK_FEATURE_1.isInverted())
      .archived(MockData.MOCK_FEATURE_1.isArchived())
      .active(MockData.MOCK_FEATURE_1.isActive())
      .customerIds(MockData.MOCK_FEATURE_1.getCustomerIds())
      .build();

    this.MOCK_CREATE_FEATURE_DTO = CreateFeatureDto.builder()
      .displayName(MockData.MOCK_CREATE_FEATURE_DTO.getDisplayName())
      .technicalName(MockData.MOCK_CREATE_FEATURE_DTO.getTechnicalName())
      .expiresOn(MockData.MOCK_CREATE_FEATURE_DTO.getExpiresOn())
      .description(MockData.MOCK_CREATE_FEATURE_DTO.getDescription())
      .inverted(MockData.MOCK_CREATE_FEATURE_DTO.isInverted())
      .customerIds(MockData.MOCK_CREATE_FEATURE_DTO.getCustomerIds())
      .build();

    this.MOCK_FEATURES_LIST = new ArrayList<>(MockData.MOCK_FEATURES);

    this.MOCK_FIRST_PAGE = MockData.getFeaeturePage(0, 10);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetAllFeatures_returnsPageOfFeatures() {
    when(featureRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(MOCK_FIRST_PAGE);

    Page<Feature> result = featureService.getAllFeatures(0, 10);

    verify(featureRepository).findAll(any(Specification.class), any(Pageable.class));
    assertEquals(MOCK_FIRST_PAGE.getContent().size(), result.getContent().size());
    assertEquals(MOCK_FIRST_PAGE.getContent(), result.getContent());
  }

  @Test
  void getFeatureById_returnsFeatureCorrectly() {
    when(featureRepository.findById(1L)).thenReturn(java.util.Optional.of(MOCK_FEATURE_1));

    Feature result = featureService.getFeatureById(1L);

    verify(featureRepository).findById(1L);
    assertEquals(MOCK_FEATURE_1, result);
  }

  @Test
  public void testGetFeatureByIdThrowsException() {
    when(featureRepository.findById(anyLong())).thenReturn(Optional.empty());

    FeatureNotFoundException exception = assertThrows(FeatureNotFoundException.class, () -> featureService.getFeatureById(1L));

    verify(featureRepository).findById(1L);
    assertEquals(FEATURE_NOT_FOUND, exception.getMessage());
  }


  @Test
  void createFeature_returnsTheCreatedFeature() {
    when(featureRepository.save(any(Feature.class))).thenReturn(MOCK_FEATURE_1);

    Feature result = featureService.createFeature(MOCK_CREATE_FEATURE_DTO);

    verify(featureRepository).save(any(Feature.class));
    assertEquals(MOCK_FEATURE_1, result);
  }

  @Test
  void deleteFeature_deletesFeatureWhenFeatureExist() {
    when(featureRepository.findById(1L)).thenReturn(Optional.of(MOCK_FEATURE_1));

    featureService.deleteFeature(1L);

    verify(featureRepository).findById(1L);
    verify(featureRepository).deleteById(1L);
  }

  @Test
  void deleteFeature_throwsWhenTheFeatureDoesNotExist() {
    when(featureRepository.findById(1L)).thenReturn(Optional.empty());

    FeatureNotFoundException exception = assertThrows(FeatureNotFoundException.class, () -> featureService.deleteFeature(1L));

    verify(featureRepository).findById(1L);
    assertEquals(FEATURE_NOT_FOUND, exception.getMessage());
  }

  @Test
  public void updateFeature_updatesFeatureCorrectly() {
    when(featureRepository.findById(anyLong())).thenReturn(Optional.of(MOCK_FEATURE_1));
    when(featureRepository.save(any(Feature.class))).thenReturn(MOCK_FEATURE_1);

    Feature updatedFeature = featureService.updateFeature(1L, MOCK_CREATE_FEATURE_DTO);

    verify(featureRepository).findById(1L);
    verify(featureRepository).save(MOCK_FEATURE_1);

    assertEquals(MOCK_CREATE_FEATURE_DTO.getDisplayName(), updatedFeature.getDisplayName());
    assertEquals(MOCK_CREATE_FEATURE_DTO.getTechnicalName(), updatedFeature.getTechnicalName());
    assertEquals(MOCK_CREATE_FEATURE_DTO.getExpiresOn(), updatedFeature.getExpiresOn());
    assertEquals(MOCK_CREATE_FEATURE_DTO.getDescription(), updatedFeature.getDescription());
    assertEquals(MOCK_CREATE_FEATURE_DTO.isInverted(), updatedFeature.isInverted());
    assertEquals(MOCK_CREATE_FEATURE_DTO.getCustomerIds(), updatedFeature.getCustomerIds());
  }

  @Test
  public void updateFeature_throwsWhenFeatureDoesNotExist() {
    when(featureRepository.findById(anyLong())).thenReturn(Optional.empty());

    FeatureNotFoundException exception = assertThrows(FeatureNotFoundException.class, () -> featureService.updateFeature(1L, MOCK_CREATE_FEATURE_DTO));

    verify(featureRepository).findById(1L);
    assertEquals(FEATURE_NOT_FOUND, exception.getMessage());
  }

  @Test
  void addCustomersForFeature_addsTheCustomersAndReturnsTheFeatureCorrectly() {
    MockData.MOCK_FEATURE_2.setCustomerIds(new ArrayList<>());
    MockData.MOCK_FEATURE_2.getCustomerIds().add(1L);

    List<Long> customerIds = List.of(2L, 3L, 4L);

    when(featureRepository.findById(2L)).thenReturn(Optional.of(MockData.MOCK_FEATURE_2));
    when(featureRepository.save(any(Feature.class))).thenReturn(MockData.MOCK_FEATURE_2);

    Feature updatedFeature = featureService.addCustomersForFeature(2L, customerIds);

    verify(featureRepository).findById(2L);
    verify(featureRepository).save(MockData.MOCK_FEATURE_2);

    assertEquals(List.of(1L, 2L, 3L, 4L), updatedFeature.getCustomerIds());
  }

  @Test
  void addCustomersForFeature_addsTheCustomersAndReturnsTheFeatureCorrectlyWhenTheCustomerIdsIsNull() {
    MockData.MOCK_FEATURE_2.setCustomerIds(null);

    List<Long> customerIds = List.of(2L, 3L, 4L);

    when(featureRepository.findById(2L)).thenReturn(Optional.of(MockData.MOCK_FEATURE_2));
    when(featureRepository.save(any(Feature.class))).thenReturn(MockData.MOCK_FEATURE_2);

    Feature updatedFeature = featureService.addCustomersForFeature(2L, customerIds);

    verify(featureRepository).findById(2L);
    verify(featureRepository).save(MockData.MOCK_FEATURE_2);

    assertEquals(customerIds, updatedFeature.getCustomerIds());
  }

  @Test
  void addFeaturesForCustomer_addsTheFeaturesToTheTheCustomerAndReturnsAllFeatures() {
    List<Long> featureIds = List.of(1L, 2L, 3L);

    when(featureRepository.findAllById(featureIds)).thenReturn(MOCK_FEATURES_LIST);
    when(featureRepository.saveAll(MOCK_FEATURES_LIST)).thenReturn(MOCK_FEATURES_LIST);

    List<Feature> updatedFeatures = featureService.addFeaturesForCustomer(1L, featureIds);

    verify(featureRepository).findAllById(featureIds);

    assertEquals(MOCK_FEATURES_LIST, updatedFeatures);
  }


  @Test
  void invertFeature_setsTheInvertedFlagToTheObjectAndReturnsCorrectly() {
    when(featureRepository.findById(1L)).thenReturn(Optional.of(MOCK_FEATURE_1));
    when(featureRepository.save(any(Feature.class))).thenReturn(MOCK_FEATURE_1);

    Feature updatedFeature = featureService.invertFeature(1L, true);

    verify(featureRepository).findById(1L);
    verify(featureRepository).save(MOCK_FEATURE_1);

    assertEquals(MOCK_FEATURE_1.isInverted(), updatedFeature.isInverted());
  }

  @Test
  void invertFeature_throwsIfFeatureWasNotFound() {
    when(featureRepository.findById(1L)).thenReturn(Optional.empty());

    FeatureNotFoundException exception = assertThrows(FeatureNotFoundException.class, () -> featureService.invertFeature(1L, true));

    verify(featureRepository).findById(1L);
    assertEquals(FEATURE_NOT_FOUND, exception.getMessage());
  }

  @Test
  void archiveFeature_archivesFeatureCorrectlyAndReturnsIt() {
    when(featureRepository.findById(1L)).thenReturn(Optional.of(MOCK_FEATURE_1));
    when(featureRepository.save(any(Feature.class))).thenReturn(MOCK_FEATURE_1);

    Feature archivedFeature = featureService.archiveFeature(1L);

    verify(featureRepository).findById(1L);
    verify(featureRepository).save(MOCK_FEATURE_1);

    assertTrue(archivedFeature.isArchived());
  }

  @Test
  void archiveFeature_throwsIfFeatureWasNotFound() {
    when(featureRepository.findById(1L)).thenReturn(Optional.empty());

    FeatureNotFoundException exception = assertThrows(FeatureNotFoundException.class, () -> featureService.archiveFeature(1L));

    verify(featureRepository).findById(1L);
    assertEquals(FEATURE_NOT_FOUND, exception.getMessage());
  }

  @Test
  void toggleFeature_togglesFeatureCorrectlyAndReturnsIt() {
    when(featureRepository.findById(1L)).thenReturn(Optional.of(MOCK_FEATURE_1));
    when(featureRepository.save(any(Feature.class))).thenReturn(MOCK_FEATURE_1);

    Feature toggledFeature = featureService.toggleFeature(1L, true);

    verify(featureRepository).findById(1L);
    verify(featureRepository).save(MOCK_FEATURE_1);

    assertTrue(toggledFeature.isActive());
  }

  @Test
  void toggleFeature_throwsIfFeatureWasNotFound() {
    when(featureRepository.findById(1L)).thenReturn(Optional.empty());

    FeatureNotFoundException exception = assertThrows(FeatureNotFoundException.class, () -> featureService.toggleFeature(1L, true));

    verify(featureRepository).findById(1L);
    assertEquals(FEATURE_NOT_FOUND, exception.getMessage());
  }
}
