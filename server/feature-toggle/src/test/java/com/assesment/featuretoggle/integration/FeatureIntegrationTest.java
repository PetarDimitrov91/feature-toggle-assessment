package com.assesment.featuretoggle.integration;

import com.assesment.featuretoggle.MockData;
import com.assesment.featuretoggle.models.feature_toggle.CreateFeatureDto;
import com.assesment.featuretoggle.models.feature_toggle.Feature;
import com.assesment.featuretoggle.services.FeatureService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Objects;

import static com.assesment.featuretoggle.utils.ExceptionMessages.FEATURE_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FeatureIntegrationTest {
  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private FeatureService featureService;

  private Feature FEAT_1;
  private Feature FEAT_2;
  private Feature FEAT_3;

  @BeforeAll
  public void setUp() {
    CreateFeatureDto feature1 = CreateFeatureDto.builder()
      .displayName("feature1")
      .technicalName("feature1")
      .expiresOn(null)
      .description("feature1")
      .inverted(false)
      .customerIds(null)
      .build();

    CreateFeatureDto feature2 = CreateFeatureDto.builder()
      .displayName("feature2")
      .technicalName("feature2")
      .expiresOn(null)
      .description("feature2")
      .inverted(false)
      .customerIds(null)
      .build();

    CreateFeatureDto feature3 = CreateFeatureDto.builder()
      .displayName("feature3")
      .technicalName("feature3")
      .expiresOn(null)
      .description("feature3")
      .inverted(false)
      .customerIds(null)
      .build();

    FEAT_1 = featureService.createFeature(feature1);
    FEAT_2 = featureService.createFeature(feature2);
    FEAT_3 = featureService.createFeature(feature3);
  }

  @Test
  @Order(1)
  public void getAllFeatures_ReturnsCorrectlyFirstPageCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").isArray())
      .andExpect(jsonPath("$.numberOfElements").value(3))
      .andExpect(jsonPath("$.content[0].id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.content[0].displayName").value(FEAT_1.getDisplayName()))
      .andExpect(jsonPath("$.content[0].technicalName").value(FEAT_1.getTechnicalName()))
      .andExpect(jsonPath("$.content[0].description").value(FEAT_1.getDescription()))
      .andExpect(jsonPath("$.content[0].inverted").value(FEAT_1.isInverted()))
      .andExpect(jsonPath("$.content[0].customerIds").isEmpty())
      .andExpect(jsonPath("$.content[1].id").value(FEAT_2.getId()))
      .andExpect(jsonPath("$.content[1].displayName").value(FEAT_2.getDisplayName()))
      .andExpect(jsonPath("$.content[1].technicalName").value(FEAT_2.getTechnicalName()))
      .andExpect(jsonPath("$.content[1].description").value(FEAT_2.getDescription()))
      .andExpect(jsonPath("$.content[1].inverted").value(FEAT_2.isInverted()))
      .andExpect(jsonPath("$.content[1].customerIds").isEmpty());
  }

  @Test
  @Order(2)
  public void getAllFeatures_ReturnsCorrectlyEmptySecondPage() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all?page=1"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").isArray())
      .andExpect(jsonPath("$.numberOfElements").value(0));
  }

  @Test
  @Order(3)
  public void getAllFeatures_ReturnsCorrectlyFirstPageWithOneElement() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/all?page=0&size=1"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").isArray())
      .andExpect(jsonPath("$.numberOfElements").value(1))
      .andExpect(jsonPath("$.content[0].id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.content[0].displayName").value(FEAT_1.getDisplayName()))
      .andExpect(jsonPath("$.content[0].technicalName").value(FEAT_1.getTechnicalName()))
      .andExpect(jsonPath("$.content[0].description").value(FEAT_1.getDescription()))
      .andExpect(jsonPath("$.content[0].inverted").value(FEAT_1.isInverted()));
  }

  @Test
  @Order(4)
  public void getFeatureById_ReturnsCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/" + FEAT_1.getId()))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.displayName").value(FEAT_1.getDisplayName()))
      .andExpect(jsonPath("$.technicalName").value(FEAT_1.getTechnicalName()))
      .andExpect(jsonPath("$.description").value(FEAT_1.getDescription()))
      .andExpect(jsonPath("$.inverted").value(FEAT_1.isInverted()));
  }

  @Test
  @Order(5)
  public void getFeatureById_throwsException() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/features/999"))
      .andDo(print())
      .andExpect(status().isNotFound())
      .andExpect(result -> assertEquals(FEATURE_NOT_FOUND, Objects.requireNonNull(result.getResolvedException()).getMessage()));
  }

  @Test
  @Order(6)
  public void createFeature_createsFeatureCorrectlyAndReturnsTheNewlyCreatedFeature() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/create")
        .contentType("application/json")
        .content("{\"displayName\":\"Test Feature 3\",\"technicalName\":\"test_feature_3\",\"description\":\"This is a test feature 3\",\"inverted\":false,\"customerIds\":[1,2,3]}"))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.displayName").value("Test Feature 3"))
      .andExpect(jsonPath("$.technicalName").value("test_feature_3"))
      .andExpect(jsonPath("$.description").value("This is a test feature 3"))
      .andExpect(jsonPath("$.inverted").value(false))
      .andExpect(jsonPath("$.customerIds").isArray())
      .andExpect(jsonPath("$.customerIds[0]").value(1))
      .andExpect(jsonPath("$.customerIds[1]").value(2))
      .andExpect(jsonPath("$.customerIds[2]").value(3));
  }

  @Test
  @Order(7)
  public void createFeature_throwsAndReturnsStatus400WhenTheBodyIsNotCorrect() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/create")
        .contentType("application/json")
        .content(MockData.MOCK_CREATE_FEATURE_DTO.toString()))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  @Test
  @Order(8)
  public void updateFeature_updatesFeatureCorrectlyAndReturnsTheUpdatedFeature() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/features/" + FEAT_1.getId())
        .contentType("application/json")
        .content("{\"displayName\":\"Test Feature 3\",\"technicalName\":\"test_feature_3\",\"description\":\"This is a test feature 3\",\"inverted\":false,\"customerIds\":[1,2,3]}"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.displayName").value("Test Feature 3"))
      .andExpect(jsonPath("$.technicalName").value("test_feature_3"))
      .andExpect(jsonPath("$.description").value("This is a test feature 3"))
      .andExpect(jsonPath("$.inverted").value(false))
      .andExpect(jsonPath("$.customerIds").isArray())
      .andExpect(jsonPath("$.customerIds[0]").value(1))
      .andExpect(jsonPath("$.customerIds[1]").value(2))
      .andExpect(jsonPath("$.customerIds[2]").value(3));
  }

  @Test
  @Order(9)
  public void updateFeature_throwsAndReturnsStatus400WhenTheBodyIsNotCorrect() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/features/" + FEAT_1.getId())
        .contentType("application/json")
        .content(MockData.MOCK_CREATE_FEATURE_DTO.toString()))
      .andDo(print())
      .andExpect(status().isBadRequest());
  }

  @Test
  @Order(10)
  public void deleteFeature_deletesFeatureCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/features/" + FEAT_3.getId()))
      .andDo(print())
      .andExpect(status().isNoContent());
  }

  @Test
  @Order(11)
  public void deleteFeature_throwsException() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/features/999"))
      .andDo(print())
      .andExpect(status().isNotFound())
      .andExpect(result -> assertEquals(FEATURE_NOT_FOUND, Objects.requireNonNull(result.getResolvedException()).getMessage()));
  }

  @Test
  @Order(12)
  public void addCustomersToFeature_addsCustomersCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/" + FEAT_1.getId() + "/addCustomers")
        .contentType("application/json")
        .content("[1,2,3]"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(FEAT_1.getId()));
  }

  @Test
  @Order(13)
  public void addCustomersToFeature_throwsException() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/999/addCustomers")
        .contentType("application/json")
        .content("[1,2,3]"))
      .andDo(print())
      .andExpect(status().isNotFound())
      .andExpect(result -> assertEquals(FEATURE_NOT_FOUND, Objects.requireNonNull(result.getResolvedException()).getMessage()));
  }

  @Test
  @Order(14)
  public void addFeaturesForCustomer_addsFeaturesCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/features/1/addFeatures")
        .contentType("application/json")
        .content("[1,2]"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[0].id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$[1].id").value(FEAT_2.getId()));
  }

  @Test
  @Order(15)
  public void invertFeature_invertsFeatureCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/" + FEAT_1.getId() + "/invert")
        .param("inverted", "true"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.inverted").value(true));
  }

  @Test
  @Order(16)
  public void invertFeature_ReturnsStatus404WhenFeatureNotFound() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/999/invert")
        .param("inverted", "true"))
      .andDo(print())
      .andExpect(status().isNotFound())
      .andExpect(result -> assertEquals(FEATURE_NOT_FOUND, Objects.requireNonNull(result.getResolvedException()).getMessage()));
  }

  @Test
  @Order(17)
  public void archiveFeature_archivesFeatureCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/" + FEAT_1.getId() + "/archive"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.archived").value(true));
  }

  @Test
  @Order(18)
  public void archiveFeature_ReturnsStatus404WhenFeatureNotFound() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/999/archive"))
      .andDo(print())
      .andExpect(status().isNotFound())
      .andExpect(result -> assertEquals(FEATURE_NOT_FOUND, Objects.requireNonNull(result.getResolvedException()).getMessage()));
  }

  @Test
  @Order(19)
  public void toggleFeature_togglesFeatureCorrectly() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/" + FEAT_1.getId() + "/toggle")
        .param("active", "true"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id").value(FEAT_1.getId()))
      .andExpect(jsonPath("$.active").value(true));
  }

  @Test
  @Order(20)
  public void toggleFeature_ReturnsStatus404WhenFeatureNotFound() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/features/999/toggle")
        .param("active", "true"))
      .andDo(print())
      .andExpect(status().isNotFound())
      .andExpect(result -> assertEquals(FEATURE_NOT_FOUND, Objects.requireNonNull(result.getResolvedException()).getMessage()));
  }
}
