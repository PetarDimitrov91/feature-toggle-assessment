import {afterNextRender, Component, inject, Renderer2} from '@angular/core';
import { RouterModule } from '@angular/router';
import {TopNavComponent} from "@feature-toggle-assessment/ui-global";
import {MatCard, MatCardContent} from "@angular/material/card";

@Component({
  standalone: true,
  imports: [RouterModule, TopNavComponent, MatCard, MatCardContent],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  readonly #renderer: Renderer2 = inject(Renderer2);

  constructor() {
    afterNextRender(() => {
      /**
       * Removes the global loader from the DOM when the application is ready.
       */
      this.#renderer.removeChild(
        document.body,
        document.querySelector('#global-loader')
      );
    });
  }}
