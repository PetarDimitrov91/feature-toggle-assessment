import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  {
    path: '',
    redirectTo: '/features',
    pathMatch: 'full',
  },
  {
    path: 'features',
    loadComponent: () =>
      import('@feature-toggle-assessment/ui-feature-toggle').then(
        (c) => c.FeaturesComponent
      ),
  },
  {
    path: 'create-feature',
    loadComponent: () =>
      import('@feature-toggle-assessment/ui-feature-toggle').then(
        (c) => c.CreateFeatureComponent
      ),
  },
  {
    path: 'feature/:id',
    loadComponent: () =>
      import('@feature-toggle-assessment/ui-feature-toggle').then(
        (c) => c.FeatureDetailsComponent
      ),
  },
];
