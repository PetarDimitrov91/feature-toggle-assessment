import {
  ChangeDetectionStrategy,
  Component,
  input,
  InputSignal,
  output,
  OutputEmitterRef,
} from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import {
  FeatureToggle,
  FeatureToggleOutput,
} from '@feature-toggle-assessment/shared';
import {
  MatCard,
  MatCardContent,
  MatCardHeader,
  MatCardImage,
  MatCardSubtitle,
  MatCardTitle,
} from '@angular/material/card';
import { RouterLink } from '@angular/router';
import {
  MatSlideToggle,
  MatSlideToggleChange,
} from '@angular/material/slide-toggle';

@Component({
  selector: 'lib-feature-toggle-list-item',
  standalone: true,
  imports: [
    CommonModule,
    MatCard,
    MatCardHeader,
    MatCardContent,
    MatCardTitle,
    MatCardSubtitle,
    RouterLink,
    MatCardImage,
    NgOptimizedImage,
    MatSlideToggle,
  ],
  templateUrl: './feature-list-item.component.html',
  styleUrl: './feature-list-item.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureListItemComponent {
  feature: InputSignal<FeatureToggle> = input.required<FeatureToggle>();
  toggle: OutputEmitterRef<FeatureToggleOutput> = output<FeatureToggleOutput>();

  toggleFeature(toggleChange: MatSlideToggleChange): void {
    this.toggle.emit({
      active: toggleChange.checked,
      featureId: this.feature().id,
      technicalName: this.feature().technicalName,
    });
  }

  prev(event: Event): void {
    event.preventDefault();
    event.stopPropagation();
  }
}
