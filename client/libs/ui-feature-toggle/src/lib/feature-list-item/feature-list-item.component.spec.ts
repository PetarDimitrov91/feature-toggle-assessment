import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FeatureListItemComponent } from './feature-list-item.component';
import { Component, ComponentRef, DebugElement } from '@angular/core';
import { provideRouter } from '@angular/router';
import { FeatureDetailsComponent } from '../feature-details/feature-details.component';
import { FeatureToggle } from '@feature-toggle-assessment/shared';
import { RouterTestingHarness } from '@angular/router/testing';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { By } from '@angular/platform-browser';

@Component({
  selector: 'lib-feature-toggle-feature-details',
  template: '',
  standalone: true,
})
class MockFeatureDetailsComponent implements Partial<FeatureDetailsComponent> {}

const mockFeatureToggle: FeatureToggle = {
  id: 1,
  technicalName: 'test',
  inverted: false,
  customerIds: [1],
  active: true,
  archived: false,
};

describe('FeatureToggleListItemComponent', () => {
  let component: FeatureListItemComponent;
  let componentRef: ComponentRef<FeatureListItemComponent>;
  let fixture: ComponentFixture<FeatureListItemComponent>;
  let debugElement: DebugElement;
  let harness: RouterTestingHarness;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureListItemComponent],
      providers: [
        provideRouter([
          { path: 'feature/:id', component: MockFeatureDetailsComponent },
        ]),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FeatureListItemComponent);
    component = fixture.componentInstance;
    componentRef = fixture.componentRef;
    debugElement = fixture.debugElement;

    componentRef.setInput('feature', mockFeatureToggle);
    harness = await RouterTestingHarness.create();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the component', () => {
    expect(debugElement.nativeElement).toBeTruthy();
  });

  it('feature input should be set', () => {
    expect(component.feature()).toEqual(mockFeatureToggle);
  });

  it('click navigates to feature details', async () => {
    const c: MockFeatureDetailsComponent = await harness.navigateByUrl(
      `/feature/${mockFeatureToggle.id}`,
      MockFeatureDetailsComponent
    );

    fixture.detectChanges();

    expect(c).toBeTruthy();
    expect(c).toBeInstanceOf(MockFeatureDetailsComponent);
  });

  it('click on mat-slide-toggle should call toggleFeature', () => {
    const toggleFeatureSpy = jest.spyOn(component, 'toggleFeature');
    const matSlideToggle = debugElement.query(By.directive(MatSlideToggle));

    matSlideToggle.triggerEventHandler('change', { checked: true });

    expect(toggleFeatureSpy).toHaveBeenCalled();
    expect(toggleFeatureSpy).toHaveBeenCalledWith({ checked: true });
  });

  it('should emit toggle event', () => {
    const toggleSpy = jest.spyOn(component.toggle, 'emit');
    const toggleChange = { checked: false } as any;

    component.toggleFeature(toggleChange);

    expect(toggleSpy).toHaveBeenCalledWith({
      active: toggleChange.checked,
      featureId: mockFeatureToggle.id,
      technicalName: mockFeatureToggle.technicalName,
    });
  });
});
