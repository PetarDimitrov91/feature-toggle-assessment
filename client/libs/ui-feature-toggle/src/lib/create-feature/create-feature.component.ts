import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  MatFormField,
  MatFormFieldModule,
  MatHint,
  MatLabel,
} from '@angular/material/form-field';
import { MatInput, MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatButton } from '@angular/material/button';
import { MatRipple, provideNativeDateAdapter } from '@angular/material/core';
import {
  CreateFeatureToggleData,
  Customer,
  CUSTOMER_DATA,
} from '@feature-toggle-assessment/shared';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { FeatureToggleService } from '@feature-toggle-assessment/domain-feature-toggle';
import { take } from 'rxjs';

interface FeatureToggleForm {
  displayName: FormControl<string>;
  technicalName: FormControl<string>;
  expiresOn: FormControl<Date>;
  description: FormControl<string>;
  inverted: FormControl<boolean>;
  customerIds: FormControl<number[]>;
}

@Component({
  selector: 'lib-create-feature',
  standalone: true,
  imports: [
    CommonModule,
    MatFormField,
    MatInput,
    MatCheckbox,
    MatButton,
    MatLabel,
    MatHint,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatSlideToggle,
    MatRipple,
  ],
  templateUrl: './create-feature.component.html',
  styleUrl: './create-feature.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideNativeDateAdapter()],
})
export class CreateFeatureComponent {
  readonly customers: Customer[] = CUSTOMER_DATA;
  readonly #featureService = inject(FeatureToggleService);

  form = new FormGroup<FeatureToggleForm>({
    displayName: new FormControl('', { nonNullable: true }),
    technicalName: new FormControl('', {
      nonNullable: true,
      validators: Validators.required,
    }),
    expiresOn: new FormControl(new Date(), { nonNullable: true }),
    description: new FormControl('', { nonNullable: true }),
    inverted: new FormControl(false, {
      nonNullable: true,
      validators: Validators.required,
    }),
    customerIds: new FormControl([], { nonNullable: true }),
  });

  onSubmit() {
    const data: CreateFeatureToggleData = this.form.getRawValue();

    if (this.form.dirty && this.form.valid) {
      this.#featureService.createFeature(data).pipe(take(1)).subscribe();
    }
  }
}
