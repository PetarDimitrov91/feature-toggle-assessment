import {
  ChangeDetectionStrategy,
  Component,
  input,
  InputSignal,
  output,
  OutputEmitterRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FeatureToggle,
  FeatureToggleOutput,
} from '@feature-toggle-assessment/shared';
import { MatLabel } from '@angular/material/form-field';
import { MatHeaderRow } from '@angular/material/table';
import { FeatureListItemComponent } from '../feature-list-item/feature-list-item.component';

@Component({
  selector: 'lib-feature-toggle-list',
  standalone: true,
  imports: [CommonModule, MatLabel, MatHeaderRow, FeatureListItemComponent],
  templateUrl: './feature-list.component.html',
  styleUrl: './feature-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureListComponent {
  features: InputSignal<Array<FeatureToggle> | null> =
    input.required<Array<FeatureToggle> | null>();
  toggle: OutputEmitterRef<FeatureToggleOutput> = output<FeatureToggleOutput>();

  onFeatureToggle(feature: FeatureToggleOutput): void {
    this.toggle.emit(feature);
  }
}
