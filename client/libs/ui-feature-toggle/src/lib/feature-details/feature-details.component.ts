import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureToggleConnector } from '@feature-toggle-assessment/api-feature-toggle';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { switchMap } from 'rxjs';

@Component({
  selector: 'lib-feature-details',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './feature-details.component.html',
  styleUrl: './feature-details.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeatureDetailsComponent {
  readonly #connector = inject(FeatureToggleConnector);

  id = input.required<number>();

  feat = toSignal(
    toObservable(this.id).pipe(
      switchMap((id: number) => this.#connector.fetchFeatureToggleById(id))
    ),
    { initialValue: null }
  );
}
