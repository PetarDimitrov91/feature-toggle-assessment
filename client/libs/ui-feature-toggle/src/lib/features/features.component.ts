import { Component, inject, Signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { toSignal } from '@angular/core/rxjs-interop';
import {
  FeatureToggle,
  FeatureToggleOutput,
} from '@feature-toggle-assessment/shared';
import { FeatureListComponent } from '../feature-list/feature-list.component';
import { FeatureToggleService } from '@feature-toggle-assessment/domain-feature-toggle';

@Component({
  selector: 'lib-ui-feature-toggle',
  standalone: true,
  imports: [CommonModule, FeatureListComponent],
  templateUrl: './features.component.html',
  styleUrl: './features.component.scss',
})
export class FeaturesComponent {
  readonly #featureService: FeatureToggleService = inject(FeatureToggleService);

  features: Signal<Array<FeatureToggle> | null> = toSignal(
    this.#featureService.features$,
    { initialValue: null }
  );

  onFeatureToggle(toggleOutput: FeatureToggleOutput): void {
    this.#featureService.toggleFeature(toggleOutput);
  }
}
