import { inject, Injectable } from '@angular/core';
import { FeatureToggleConnector } from '@feature-toggle-assessment/api-feature-toggle';
import {
  CreateFeatureToggleData,
  FeatureToggle,
  FeatureToggleOutput,
  Page,
  PageRequest,
} from '@feature-toggle-assessment/shared';
import {
  BehaviorSubject,
  catchError,
  Observable,
  Subject,
  switchMap,
  take,
  tap,
} from 'rxjs';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { ThemeManagerService } from '@feature-toggle-assessment/ui-global';

@Injectable({
  providedIn: 'root',
})
export class FeatureToggleService {
  readonly THEMING_FEATURE_ID = 'ui-theming';
  readonly #connector: FeatureToggleConnector = inject(FeatureToggleConnector);
  readonly #themeManager: ThemeManagerService = inject(ThemeManagerService);
  #featureToggles$ = new BehaviorSubject<Array<FeatureToggle> | null>(null);
  fetchFeatures$ = new Subject<PageRequest>();

  constructor() {
    this.fetchFeatures$
      .pipe(
        switchMap((pageRequest: PageRequest) =>
          this.#connector.fetchPageOfFeatureToggles(
            pageRequest.page,
            pageRequest.size
          )
        ),
        catchError((e) => {
          console.error('Error fetching feature toggles', e);
          return [];
        }),
        takeUntilDestroyed()
      )
      .subscribe({
        next: (page: Page<FeatureToggle>) => {
          this.setFeatures(page.content);
          this.checkForThemingEnabled(page.content);
        },
      });

    //fetch first page of features, later we can create more consistent pagination
    this.fetchFeatures$.next({ page: 0, size: 10 });
  }

  private checkForThemingEnabled(features: Array<FeatureToggle>) {
    //some dummy check to see if theming is enabled
    //here we can also check if feature is enabled for the current user. For now, we just check if the feature is enabled as we do no have authentication

    const themeEnabled = features?.some(
      (feat: FeatureToggle): boolean =>
        feat.technicalName === this.THEMING_FEATURE_ID && feat.active
    );

    themeEnabled
      ? this.#themeManager.enableFeature()
      : this.#themeManager.disableFeature();
  }

  get features$(): Observable<Array<FeatureToggle> | null> {
    return this.#featureToggles$.asObservable();
  }

  private setFeatures(featureToggles: Array<FeatureToggle>) {
    const crrFeatState = this.#featureToggles$.getValue();

    this.#featureToggles$.next([...(crrFeatState ?? []), ...featureToggles]);
  }

  toggleFeature(toggleOutput: FeatureToggleOutput): void {
    if (toggleOutput.technicalName === this.THEMING_FEATURE_ID) {
      toggleOutput.active
        ? this.#themeManager.enableFeature()
        : this.#themeManager.disableFeature();
    }

    this.#connector
      .toggleFeature(toggleOutput)
      .pipe(take(1))
      .subscribe({
        next: (feature: FeatureToggle) => {
          const crrFeatState: Array<FeatureToggle> =
            this.#featureToggles$.getValue() ?? [];

          const updatedFeatures: Array<FeatureToggle> = crrFeatState.map(
            (feat: FeatureToggle): FeatureToggle =>
              feat.id === feature.id ? feature : feat
          );

          this.#featureToggles$.next(updatedFeatures);
        },
      });
  }

  createFeature(feature: CreateFeatureToggleData): Observable<FeatureToggle> {
    return this.#connector.createFeatureToggle(feature).pipe(
      tap((feat: FeatureToggle): void => {
        const crrFeatState: Array<FeatureToggle> =
          this.#featureToggles$.getValue() ?? [];
        this.#featureToggles$.next([...crrFeatState, feat]);
      })
    );
  }
}
