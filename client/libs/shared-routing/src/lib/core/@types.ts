export type AppRoute = {
  name: string;
  path: '/' | '/features' | '/create-feature' | '/feature/:id';
};
