import { AppRoute } from './@types';

export const navRoutes: ReadonlyArray<AppRoute> = [
  {
    path: '/features',
    name: 'Features',
  },
  {
    path: '/create-feature',
    name: 'Create Feature Toggle',
  },
];
