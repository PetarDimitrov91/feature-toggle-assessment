import { TestBed } from '@angular/core/testing';
import { provideAuthUrl, provideBaseUrl } from '../misc/base-url.conf';
import { UrlBuilder } from './url-builder';

function setup(baseUrl?: string, authUrl?: string) {
  TestBed.configureTestingModule({
    providers: [
      baseUrl ? provideBaseUrl(baseUrl) : [],
      authUrl ? provideAuthUrl(authUrl) : [],
    ],
  });

  const service = TestBed.inject(UrlBuilder);

  return { service };
}

describe('UrlBuilderService', () => {
  it('should be created', () => {
    expect(setup().service).toBeTruthy();
  });

  it('should return null for baseUrl if not provied', () => {
    expect(setup().service.baseUrl).toBeNull();
  });
});

describe('UrlBuilder with baseUrl', () => {
  let service: UrlBuilder;
  const BASE_URL = 'http://localhost:3000';

  beforeEach(() => {
    const { service: s } = setup(BASE_URL);
    service = s;
  });

  it('should return baseUrl', () => {
    expect(service.baseUrl).toBe(BASE_URL);
  });

  it('should build url with path', () => {
    expect(service.buildUrl('path')).toBe(`${BASE_URL}/path`);
  });

  it('should build url with longer path', () => {
    expect(service.buildUrl('path/path2')).toBe(`${BASE_URL}/path/path2`);
  });

  it('should build url with path and url params', () => {
    const path = 'path/${id}';
    const expected = `${BASE_URL}/path/1`;
    expect(service.buildUrl(path, { urlParams: { id: '1' } })).toBe(expected);
  });

  it('should build url with more than on path params', () => {
    const path = 'path/${id}/path2/${cartId}';
    const expected = `${BASE_URL}/path/1/path2/2`;
    expect(
      service.buildUrl(path, { urlParams: { id: '1', cartId: '2' } })
    ).toBe(expected);
  });

  it('should build url with query params', () => {
    const path = 'path';
    const expected = `${BASE_URL}/path?id=1`;
    expect(service.buildUrl(path, { queryParams: { id: '1' } })).toBe(expected);
  });

  it('should build url with more than one query params', () => {
    const path = 'path';
    const expected = `${BASE_URL}/path?id=1&cartId=2`;
    expect(
      service.buildUrl(path, { queryParams: { id: '1', cartId: '2' } })
    ).toBe(expected);
  });

  it('should build url with path and query params', () => {
    const path = 'path/${id}';
    const expected = `${BASE_URL}/path/1?id=1`;
    expect(
      service.buildUrl(path, {
        urlParams: { id: '1' },
        queryParams: { id: '1' },
      })
    ).toBe(expected);
  });

  it('should build complex url with more the one path and query params', () => {
    const path = 'path/${id}/path2/${cartId}';
    const expected = `${BASE_URL}/path/1/path2/2?id=1&cartId=2`;
    expect(
      service.buildUrl(path, {
        urlParams: { id: '1', cartId: '2' },
        queryParams: { id: '1', cartId: '2' },
      })
    ).toBe(expected);
  });
});

//write the same tests like those above but for the authUrl
describe('UrlBuilder with authUrl', () => {
  let service: UrlBuilder;
  const AUTH_URL = 'http://localhost:3000/auth';
  const BASE_URL = 'http://localhost:3000';

  beforeEach(() => {
    const { service: s } = setup(BASE_URL, AUTH_URL);
    service = s;
  });

  it('should return authUrl', () => {
    expect(service.authUrl).toBe(AUTH_URL);
  });

  it('should build auth url with path', () => {
    expect(service.buildAuthUrl('path')).toBe(`${AUTH_URL}/path`);
  });

  it('should build auth url with longer path', () => {
    expect(service.buildAuthUrl('path/path2')).toBe(`${AUTH_URL}/path/path2`);
  });

  it('should build auth url with path and url params', () => {
    const path = 'path/${id}';
    const expected = `${AUTH_URL}/path/1`;
    expect(service.buildAuthUrl(path, { urlParams: { id: '1' } })).toBe(
      expected
    );
  });

  it('should build auth url with more than on path params', () => {
    const path = 'path/${id}/path2/${cartId}';
    const expected = `${AUTH_URL}/path/1/path2/2`;
    expect(
      service.buildAuthUrl(path, { urlParams: { id: '1', cartId: '2' } })
    ).toBe(expected);
  });

  it('should build auth url with query params', () => {
    const path = 'path';
    const expected = `${AUTH_URL}/path?id=1`;
    expect(service.buildAuthUrl(path, { queryParams: { id: '1' } })).toBe(
      expected
    );
  });

  it('should build auth url with more than one query params', () => {
    const path = 'path';
    const expected = `${AUTH_URL}/path?id=1&cartId=2`;
    expect(
      service.buildAuthUrl(path, { queryParams: { id: '1', cartId: '2' } })
    ).toBe(expected);
  });

  it('should build auth url with path and query params', () => {
    const path = 'path/${id}';
    const expected = `${AUTH_URL}/path/1?id=1`;
    expect(
      service.buildAuthUrl(path, {
        urlParams: { id: '1' },
        queryParams: { id: '1' },
      })
    ).toBe(expected);
  });

  it('should build complex auth url with more the one path and query params', () => {
    const path = 'path/${id}/path2/${cartId}';
    const expected = `${AUTH_URL}/path/1/path2/2?id=1&cartId=2`;
    expect(
      service.buildAuthUrl(path, {
        urlParams: { id: '1', cartId: '2' },
        queryParams: { id: '1', cartId: '2' },
      })
    ).toBe(expected);
  });

  it('buildAuthUrl should throw error if authUrl is not provided', () => {
    TestBed.resetTestingModule();
    expect(() => setup(BASE_URL).service.buildAuthUrl('path')).toThrow(
      'AUTH_URL is not provided. Make sure to provide it in your application.'
    );
  });
});
