export interface UrlOptions {
  urlParams?: Record<string, string>;
  queryParams?: Record<string, string>;
}
