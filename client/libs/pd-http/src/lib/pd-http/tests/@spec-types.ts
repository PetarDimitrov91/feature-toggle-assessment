export type ArrayBufferResponse = ArrayBuffer;

export type BlobResponse = Blob;

export type ObjectResponse = Record<string, never>;

export interface TextResponse {
  key: string;
  id: number;
}

export interface CustomResponse {
  id: number;
  name: string;
}
