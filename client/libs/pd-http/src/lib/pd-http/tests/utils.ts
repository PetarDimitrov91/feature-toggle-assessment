import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';

export const URL = 'https://api.example.com/path/to/resource';

export const progressiveEvent = new ProgressEvent('Network error');

export const expectedErrorResponse = new HttpErrorResponse({
  error: progressiveEvent,
  status: 500,
  statusText: 'Server Error',
  url: URL,
});

const _headers = new HttpHeaders();
_headers.append('Content-Type', 'application/json');
_headers.append('Authorization', 'Bearer token');

export const headers = _headers;
