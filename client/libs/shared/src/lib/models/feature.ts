interface AbstractEntity {
  id: number;
}

interface BaseFeature extends AbstractEntity {
  displayName?: string;
  technicalName: string;
  expiresOn?: Date | null;
  description?: string;
  inverted: boolean;
  customerIds: number[];
}

export interface FeatureToggle extends BaseFeature {
  archived: boolean;
  active: boolean;
}

export type FeatureToggleOutput = {
  active: boolean;
  featureId: number;
  technicalName: string;
};

export type CreateFeatureToggleData = Omit<BaseFeature, 'id'>;

export interface Customer extends AbstractEntity {
  name: string;
}

export const CUSTOMER_DATA: Customer[] = [
  { id: 1, name: 'Customer 1' },
  { id: 2, name: 'Customer 2' },
  { id: 3, name: 'Customer 3' },
  { id: 4, name: 'Customer 4' },
  { id: 5, name: 'Customer 6' },
  { id: 6, name: 'Customer 7' },
  { id: 7, name: 'Customer 8' },
  { id: 8, name: 'Customer 9' },
  { id: 9, name: 'Customer 10' },
];
