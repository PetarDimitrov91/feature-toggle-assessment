import { provideBaseUrl } from '@feature-toggle-assessment/pd-http';
import {
  ClassProvider,
  ConstructorProvider,
  EnvironmentProviders,
  ExistingProvider,
  FactoryProvider,
  TypeProvider,
  ValueProvider,
} from '@angular/core';
import {
  provideHttpClient,
  withInterceptors,
  withJsonpSupport,
} from '@angular/common/http';

const BASE_URL = 'http://localhost:8080/api/v1/features';

export function provideFeatureToggleApi(): (
  | TypeProvider
  | ValueProvider
  | ClassProvider
  | ConstructorProvider
  | ExistingProvider
  | FactoryProvider
  | any[]
  | EnvironmentProviders
)[] {
  return [
    provideBaseUrl(BASE_URL),
    provideHttpClient(withInterceptors([]), withJsonpSupport()),
  ];
}
