import { TestBed } from '@angular/core/testing';

import { FeatureToggleConnector } from './feature-toggle-connector.service';

describe('FeatureToggleConnectorService', () => {
  let service: FeatureToggleConnector;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeatureToggleConnector);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
