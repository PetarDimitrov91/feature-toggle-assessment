import { inject, Injectable } from '@angular/core';
import { UrlBuilder } from '@feature-toggle-assessment/pd-http';
import {
  CreateFeatureToggleData,
  FeatureToggle,
  FeatureToggleOutput,
  Page,
} from '@feature-toggle-assessment/shared';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class FeatureToggleConnector {
  readonly #urlBuilder: UrlBuilder = inject(UrlBuilder);
  readonly #http: HttpClient = inject(HttpClient);

  /**
   * Fetches a page of feature toggles.
   *
   * @param {number} page - The page number to fetch.
   * @param {number} size - The number of feature toggles per page.
   * @returns {Observable<Page<FeatureToggle>>} An Observable that emits the page of feature toggles.
   */
  fetchPageOfFeatureToggles(
    page: number,
    size: number
  ): Observable<Page<FeatureToggle>> {
    const url: string = this.#urlBuilder.buildUrl('all', {
      queryParams: { page: page.toString(), size: size.toString() },
    });

    return this.#http.get<Page<FeatureToggle>>(url);
  }

  toggleFeature(toggleOutput: FeatureToggleOutput): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl('${id}/toggle', {
      urlParams: { id: toggleOutput.featureId.toString() },
      queryParams: { active: toggleOutput.active.toString() },
    });

    return this.#http.patch<FeatureToggle>(url, null);
  }

  /**
   * Fetches a feature toggle by its ID.
   *
   * @param {number} featureId - The ID of the feature toggle to fetch.
   * @returns {Observable<FeatureToggle>} An Observable that emits the feature toggle.
   */
  fetchFeatureToggleById(featureId: number): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl('${id}', {
      urlParams: { id: featureId.toString() },
    });

    return this.#http.get<FeatureToggle>(url);
  }

  /**
   * Creates a new feature toggle.
   *
   * @param {FeatureToggle} feature - The feature toggle to create.
   * @returns {Observable<FeatureToggle>} An Observable that emits the created feature toggle.
   */
  createFeatureToggle(
    feature: CreateFeatureToggleData
  ): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl('create');

    return this.#http.post<FeatureToggle>(url, feature);
  }

  /**
   * Updates a feature toggle.
   *
   * @param {FeatureToggle} featureToggle - The feature toggle to update.
   * @returns {Observable<FeatureToggle>} An Observable that emits the updated feature toggle.
   */
  updateFeatureToggle(featureToggle: FeatureToggle): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl('${id}', {
      urlParams: { id: featureToggle.id.toString() },
    });

    return this.#http.put<FeatureToggle>(url, featureToggle);
  }

  /**
   * Deletes a feature toggle.
   *
   * @param {number} featureId - The ID of the feature toggle to delete.
   * @returns {Observable<void>} An Observable that completes when the feature toggle is deleted.
   */
  deleteFeatureToggle(featureId: number): Observable<void> {
    const url: string = this.#urlBuilder.buildUrl('${id}', {
      urlParams: { id: featureId.toString() },
    });

    return this.#http.delete<void>(url);
  }

  /**
   * Adds customers to a feature toggle.
   *
   * @param {number} featureToggleId - The ID of the feature toggle.
   * @param {number[]} customerIds - The IDs of the customers to add to the feature toggle.
   * @returns {Observable<FeatureToggle>} An Observable that emits the updated feature toggle.
   */
  addCustomersToFeatureToggle(
    featureToggleId: number,
    customerIds: number[]
  ): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl(
      '/${featureId}/addCustomers',
      {
        urlParams: { featureId: featureToggleId.toString() },
      }
    );

    return this.#http.post<FeatureToggle>(url, customerIds);
  }

  /**
   * Adds features to a customer.
   *
   * @param {number} customerId - The ID of the customer.
   * @param {number[]} featureIds - The IDs of the features to add to the customer.
   * @returns {Observable<Array<FeatureToggle>>} An Observable that emits the updated features.
   */
  addFeatureToCustomer(
    customerId: number,
    featureIds: number[]
  ): Observable<Array<FeatureToggle>> {
    const url: string = this.#urlBuilder.buildUrl('${customerId}/addFeatures', {
      urlParams: { customerId: customerId.toString() },
    });

    return this.#http.post<Array<FeatureToggle>>(url, featureIds);
  }

  /**
   * Inverts a feature toggle.
   *
   * @param {number} featureId - The ID of the feature toggle to invert.
   * @param {boolean} inverted - The new inverted state of the feature toggle.
   * @returns {Observable<FeatureToggle>} An Observable that emits the updated feature toggle.
   */
  invertFeatureToggle(
    featureId: number,
    inverted: boolean
  ): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl('/${id}/invert', {
      urlParams: { id: featureId.toString() },
    });

    return this.#http.patch<FeatureToggle>(url, inverted);
  }

  /**
   * Archives a feature toggle.
   *
   * @param {number} featureId - The ID of the feature toggle to archive.
   * @returns {Observable<FeatureToggle>} An Observable that emits the archived feature toggle.
   */
  archiveFeatureToggle(featureId: number): Observable<FeatureToggle> {
    const url: string = this.#urlBuilder.buildUrl('${id}/archive', {
      urlParams: { id: featureId.toString() },
    });

    return this.#http.patch<FeatureToggle>(url, null);
  }
}
