import { inject, PLATFORM_INITIALIZER, Provider } from '@angular/core';
import { ThemeManagerService } from './theme-manager.service';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

export function provideTheming(): Provider[] {
  return [
    {
      provide: PLATFORM_INITIALIZER,
      useFactory: () => inject(ThemeManagerService).theme$,
      multi: true,
    },
    {
      provide: MATERIAL_SANITY_CHECKS,
      useValue: {
        theme: false,
      },
    },
  ];
}
