import {afterNextRender, inject, Injectable, Renderer2, RendererFactory2,} from '@angular/core';
import {BehaviorSubject, combineLatest, fromEvent, map, Observable, shareReplay, startWith, Subject, tap,} from 'rxjs';
import {AVAILABLE_THEMES, Theme, THEME_COOKIE_KEY, ThemeCode, THEMES, ThemeState,} from './themes';
import {CookieService} from 'ngx-cookie-service';
import {DOCUMENT} from '@angular/common';

/**
 * ThemeManagerService is a service that manages the theme of the application.
 * It provides methods to get and set the user's preferred theme and dark mode setting.
 * It also provides methods to initialize the user's preferred theme and dark mode setting from a cookie.
 * The service uses a BehaviorSubject to keep track of the current theme.
 */
@Injectable({
  providedIn: 'root',
})
export class ThemeManagerService {
  readonly #cookieService: CookieService = inject(CookieService);
  readonly #rendererFactory: RendererFactory2 = inject(RendererFactory2);
  readonly #renderer: Renderer2 = this.#rendererFactory.createRenderer(
    null,
    null
  );
  readonly #DOC: Document = inject(DOCUMENT);
  readonly #featureEnabled = new BehaviorSubject<boolean>(false);

  #defTheme: Theme =
    inject(THEMES).find((t: Theme) => t.default) ??
    AVAILABLE_THEMES['magenta-violet'];

  #usrPrefersDark = new Subject<boolean | null>();
  #usrPrefersTheme = new Subject<Theme | null>();
  #theme$ = new BehaviorSubject<Theme>(this.#defTheme);

  constructor() {
    afterNextRender(() => {
      const sysPerfQuery: MediaQueryList = matchMedia(
        `(prefers-color-scheme: dark)`
      );

      const sysPrefersDark$: Observable<boolean> = fromEvent<MediaQueryList>(
        sysPerfQuery,
        'change'
      ).pipe(
        startWith(sysPerfQuery),
        map((e: MediaQueryList) => e.matches)
      );

      this.setupTheming(sysPrefersDark$);

      this.#usrPrefersDark.next(this.initializeUsrPrefersDarkMode());
      this.#usrPrefersTheme.next(this.initializeUsrPrefersTheme());
    });
  }

  /**
   * Returns an Observable that emits the current theme.
   */
  get theme$(): Observable<Theme> {
    return this.#theme$.asObservable().pipe(shareReplay());
  }

  /**
   * Returns an Observable that emits the user's preferred dark mode setting.
   */
  get usrPrefersDarkMode$(): Observable<boolean | null> {
    return this.#usrPrefersDark.asObservable().pipe(shareReplay());
  }

  get featureEnabled$(): Observable<boolean> {
    return this.#featureEnabled.asObservable().pipe(shareReplay());
  }

  /**
   * Sets the user's preferred dark mode setting.
   */
  set usrPrefersDarkMode(v: boolean) {
    this.#usrPrefersDark.next(v);
  }

  /**
   * Sets the user's preferred theme.
   */
  set usrPrefersTheme(v: Theme) {
    this.#usrPrefersTheme.next(v);
  }

  /**
   * Enables the feature.
   */
  enableFeature(): void {
    const crrValue: boolean = this.#featureEnabled.getValue();
    if (!crrValue) this.#featureEnabled.next(true);
  }

  /**
   * Disables the feature.
   */
  disableFeature(): void {
    const crrValue: boolean = this.#featureEnabled.getValue();
    if (crrValue) this.#featureEnabled.next(false);
  }

  /**
   * Initializes the user's preferred dark mode setting from a cookie.
   */
  private initializeUsrPrefersDarkMode(): boolean | null {
    const cookie: string = this.#cookieService.get(THEME_COOKIE_KEY);
    const crrThemeState: ThemeState = JSON.parse(cookie || '{}') as ThemeState;

    return crrThemeState[THEME_COOKIE_KEY]?.darkMode ?? null;
  }

  /**
   * Initializes the user's preferred theme from a cookie.
   */
  private initializeUsrPrefersTheme(): Theme | null {
    const cookie: string = this.#cookieService.get(THEME_COOKIE_KEY);
    const crrThemeState: ThemeState = JSON.parse(cookie || '{}') as ThemeState;
    const code: ThemeCode | null =
      crrThemeState[THEME_COOKIE_KEY]?.code ?? null;

    if (!code) return null;

    return AVAILABLE_THEMES[code];
  }

  /**
   * Sets up theming for the application.
   */
  private setupTheming(sysPrefersDark$: Observable<boolean>): void {
    const usrPrefersTheme: Observable<Theme | null> =
      this.#usrPrefersTheme.pipe(
        tap((t: Theme | null) => this.saveThemeState(t))
      );

    const usrPrefersDark: Observable<boolean | null> =
      this.#usrPrefersDark.pipe(
        tap((d: boolean | null) => this.saveThemeState(d))
      );

    combineLatest([
      usrPrefersTheme,
      usrPrefersDark,
      sysPrefersDark$,
      this.#featureEnabled,
    ])
      .pipe(
        map(
          ([
            usrPreferredTheme,
            usrPrefersDark,
            sysPrefersDark,
            featureEnabled,
          ]) => {
            const theme: Theme = featureEnabled
              ? usrPreferredTheme ?? this.#defTheme
              : AVAILABLE_THEMES.default;

            this.loadTheme(
              this.getThemeLinkElement(),
              theme,
              featureEnabled ? usrPrefersDark ?? sysPrefersDark : false
            );

            this.#theme$.next(theme);
            return theme;
          }
        )
      )
      .subscribe();
  }

  /**
   * Saves the user's preferred theme or dark mode setting to a cookie.
   */
  private saveThemeState(theme: Theme | null): void;
  private saveThemeState(isDark: boolean | null): void;
  private saveThemeState(themeOrIsDark: Theme | boolean | null): void {
    if (themeOrIsDark === null) return;

    const cookie: string = this.#cookieService.get(THEME_COOKIE_KEY);
    const crrThemeState: ThemeState = JSON.parse(cookie || '{}') as ThemeState;
    let newThemeState: ThemeState;

    if (typeof themeOrIsDark === 'boolean') {
      newThemeState = {
        [THEME_COOKIE_KEY]: {
          ...crrThemeState[THEME_COOKIE_KEY],
          darkMode: themeOrIsDark,
        },
      };
    } else {
      newThemeState = {
        [THEME_COOKIE_KEY]: {
          ...crrThemeState[THEME_COOKIE_KEY],
          code: themeOrIsDark.code,
        },
      };
    }

    this.#cookieService.set(THEME_COOKIE_KEY, JSON.stringify(newThemeState));
  }

  /**
   * Returns the link element for the theme stylesheet.
   */
  private getThemeLinkElement(): HTMLLinkElement {
    const existingLinkEl: HTMLLinkElement | null =
      this.#DOC.head.querySelector<HTMLLinkElement>(`#appTheme`);

    if (existingLinkEl) {
      return existingLinkEl;
    }

    const linkEl = this.#renderer.createElement('link');
    linkEl.setAttribute('rel', 'stylesheet');
    linkEl.setAttribute('id', 'appTheme');

    this.#renderer.appendChild(this.#DOC.head, linkEl);

    return linkEl;
  }

  /**
   * Loads the theme stylesheet into the link element.
   */
  private loadTheme(
    linkEl: HTMLLinkElement,
    theme: Theme,
    isDarkMode: boolean
  ) {
    this.#renderer.setAttribute(
      linkEl,
      'href',
      this.resolveThemeUrl(theme, isDarkMode)
    );
  }

  /**
   * Returns the URL of the theme stylesheet.
   */
  private resolveThemeUrl(themeName: Theme, isDarkMode: boolean) {
    return `${isDarkMode ? 'dark' : 'light'}-${themeName.fileName}`;
  }
}
