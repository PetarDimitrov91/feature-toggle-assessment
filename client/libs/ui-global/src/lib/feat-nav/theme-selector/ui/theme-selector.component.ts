import {
  afterNextRender,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  inject,
  Signal,
  viewChild,
} from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { MatIconButton } from '@angular/material/button';
import { MatMenu, MatMenuItem, MatMenuTrigger } from '@angular/material/menu';
import { MatIcon } from '@angular/material/icon';
import { moonSvg, sunSvg, Theme, THEMES } from '../core/themes';
import { MatRadioButton, MatRadioGroup } from '@angular/material/radio';
import { MatRipple } from '@angular/material/core';
import {
  MatSlideToggleChange,
  MatSlideToggleModule,
} from '@angular/material/slide-toggle';
import { ThemeManagerService } from '../core/theme-manager.service';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'spc-ui-theme-selector',
  standalone: true,
  imports: [
    CommonModule,
    MatIconButton,
    MatMenuTrigger,
    MatIcon,
    MatMenu,
    MatMenuItem,
    MatRadioGroup,
    MatRadioButton,
    NgOptimizedImage,
    MatRipple,
    MatSlideToggleModule,
  ],
  templateUrl: './theme-selector.component.html',
  styleUrl: './theme-selector.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemeSelectorComponent {
  themes: ReadonlyArray<Theme> = inject(THEMES);
  readonly #themeManager: ThemeManagerService = inject(ThemeManagerService);
  #defTheme: Theme =
    this.themes.find((t: Theme) => t.default) ?? this.themes[0];

  lightDarkSwitchRef: Signal<ElementRef> = viewChild.required(
    'darkModeSwitch',
    {
      read: ElementRef,
    }
  );

  crrTheme: Signal<Theme> = toSignal(this.#themeManager.theme$, {
    initialValue: this.#defTheme,
  });

  usrPrefersDarkMode: Signal<boolean | null> = toSignal(
    this.#themeManager.usrPrefersDarkMode$,
    {
      initialValue: null,
    }
  );

  featureEnabled: Signal<boolean> = toSignal(
    this.#themeManager.featureEnabled$,
    {
      initialValue: false,
    }
  );

  constructor() {
    afterNextRender(() => {
      if (this.featureEnabled()) {
        this.attachSvgToElement('.mdc-switch__icon--on', sunSvg);
        this.attachSvgToElement('.mdc-switch__icon--off', moonSvg);
      }
    });
  }

  attachSvgToElement(className: string, svg: string) {
    this.lightDarkSwitchRef()
      .nativeElement.querySelector(className)
      .firstChild.setAttribute('d', svg);
  }

  selectTheme(event: MouseEvent, theme: Theme) {
    if (theme.code === this.crrTheme().code) {
      event.preventDefault();
      event.stopPropagation();
      return;
    }

    this.#themeManager.usrPrefersTheme = theme;
  }

  changeLightDarkMode(toggleChange: MatSlideToggleChange) {
    this.#themeManager.usrPrefersDarkMode = toggleChange.checked;
  }
}
