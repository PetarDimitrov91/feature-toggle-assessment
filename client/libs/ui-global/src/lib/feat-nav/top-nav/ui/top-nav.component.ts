import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbar } from '@angular/material/toolbar';
import { NavLinksComponent } from '../../nav-links/ui/nav-links.component';
import { ThemeSelectorComponent } from '../../theme-selector/ui/theme-selector.component';
import {AppRoute, navRoutes} from "@feature-toggle-assessment/shared-routing";

@Component({
  selector: 'spc-ui-nav, nav[pdNav]',
  standalone: true,
  imports: [
    CommonModule,
    MatToolbar,
    NavLinksComponent,
    ThemeSelectorComponent,
  ],
  templateUrl: './top-nav.component.html',
  styleUrl: './top-nav.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopNavComponent {
  routes: ReadonlyArray<AppRoute> = navRoutes;
}
