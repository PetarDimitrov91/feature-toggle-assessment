import {
  ChangeDetectionStrategy,
  Component,
  input,
  InputSignal,
} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatAnchor} from '@angular/material/button';
import {RouterLink, RouterLinkActive} from '@angular/router';
import {AppRoute} from "@feature-toggle-assessment/shared-routing";

@Component({
  selector: 'spc-ui-nav-links',
  standalone: true,
  imports: [CommonModule, MatAnchor, RouterLink, RouterLinkActive],
  templateUrl: './nav-links.component.html',
  styleUrl: './nav-links.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavLinksComponent {
  routes: InputSignal<ReadonlyArray<AppRoute>> =
    input.required<ReadonlyArray<AppRoute>>();
}
